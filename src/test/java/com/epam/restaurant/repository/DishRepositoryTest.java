package com.epam.restaurant.repository;

import com.epam.restaurant.modelFactory.ModelEnum;

public class DishRepositoryTest extends AbstractRepositoryTest<DishRepository> {
    public DishRepositoryTest() {
        super(ModelEnum.DISH);
    }
}
