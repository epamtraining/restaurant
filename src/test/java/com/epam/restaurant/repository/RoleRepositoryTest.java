package com.epam.restaurant.repository;

import com.epam.restaurant.modelFactory.ModelEnum;

public class RoleRepositoryTest extends AbstractRepositoryTest<RoleRepository> {
    public RoleRepositoryTest() {
        super(ModelEnum.ROLE);
    }
}
