package com.epam.restaurant.repository;

import com.epam.restaurant.modelFactory.ModelEnum;

public class BillRepositoryTest extends AbstractRepositoryTest<BillRepository> {
    public BillRepositoryTest() {
        super(ModelEnum.BILL);
    }
}
