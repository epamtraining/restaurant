package com.epam.restaurant.repository;

import com.epam.restaurant.modelFactory.ModelEnum;

public class OrderOfUserRepositoryTest extends AbstractRepositoryTest<OrderOfUserRepository> {

    public OrderOfUserRepositoryTest() {
        super(ModelEnum.ORDER_OF_USER);
    }
}
