package com.epam.restaurant.repository;

import com.epam.restaurant.modelFactory.ModelEnum;

public class ChoosenDishRepositoryTest extends AbstractRepositoryTest<ChoosenDishRepository> {
    public ChoosenDishRepositoryTest() {
        super(ModelEnum.CHOOSEN_DISH);
    }
}
