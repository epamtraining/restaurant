package com.epam.restaurant.repository;

import com.epam.restaurant.modelFactory.ModelEnum;

public class CategoryOfDishTest extends AbstractRepositoryTest<CategoryOfDishRepository> {
    public CategoryOfDishTest() {
        super(ModelEnum.CATEGORY_OF_DISH);
    }
}
