package com.epam.restaurant.repository;


import com.epam.restaurant.modelFactory.ModelEnum;

public class RoleOfUserRepositoryTest extends AbstractRepositoryTest<RoleOfUserRepository> {
    public RoleOfUserRepositoryTest() {
        super(ModelEnum.ROLE_OF_USER);
    }

}
