package com.epam.restaurant.repository;

import com.epam.restaurant.model.*;
import com.epam.restaurant.modelFactory.ModelEnum;
import com.epam.restaurant.modelFactory.ModelFactory;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.junit4.SpringRunner;

@Getter
@NoArgsConstructor
@RunWith(SpringRunner.class)
@DataJpaTest
@Ignore
public abstract class AbstractRepositoryTest<R extends CrudRepository> {

    @Autowired
    private R r;

    private ModelEnum modelEnum;

    protected AbstractRepositoryTest(ModelEnum modelEnum) {
        this.modelEnum = modelEnum;
    }

    @Autowired
    private TestEntityManager entityManager;


    @Test
    public void testSaveEntityToDB(){
        Object save = null;
        Model model = ModelFactory.getModel(modelEnum);
        if (model instanceof RoleOfUser){
            RoleOfUser roleOfUser = (RoleOfUser) model;
            persistRoleOfUser(roleOfUser);
            save = r.save(roleOfUser);
        } else if (model instanceof OrderOfUser){
            OrderOfUser orderOfUser = (OrderOfUser) model;
            persistOrderOfUser(orderOfUser);
            save = r.save(orderOfUser);
        } else if (model instanceof Dish){
            Dish dish = (Dish) model;
            persistDish(dish);
            save = r.save(dish);
        } else if (model instanceof ChoosenDish){
            ChoosenDish choosenDish = (ChoosenDish)model;
            persistChoosenDish(choosenDish);
            save = r.save(choosenDish);
        } else if (model instanceof Bill){
            Bill bill = (Bill) model;
            persistBill(bill);
            save = r.save(bill);
        } else {
            save = r.save(model);
        }
        assert (save != null);
    }


    @Test
    public void testDeleteEntityFromDB(){
        Model model = ModelFactory.getModel(modelEnum);
        Long persistId = null;
        if (model instanceof RoleOfUser){
            RoleOfUser roleOfUser = (RoleOfUser) model;
            persistRoleOfUser(roleOfUser);
            persistId = (Long) entityManager.persistAndGetId(model);
            r.delete(model);
        } else if (model instanceof OrderOfUser){
            OrderOfUser orderOfUser = (OrderOfUser) model;
            persistOrderOfUser(orderOfUser);
            persistId = (Long) entityManager.persistAndGetId(model);
            r.delete(model);
        } else if (model instanceof Dish){
            Dish dish = (Dish) model;
            persistDish(dish);
            persistId = (Long) entityManager.persistAndGetId(model);
            r.delete(model);
        } else if (model instanceof ChoosenDish){
            ChoosenDish choosenDish = (ChoosenDish)model;
            persistChoosenDish(choosenDish);
            persistId = (Long) entityManager.persistAndGetId(model);
            r.delete(model);
        } else if (model instanceof Bill){
            Bill bill = (Bill) model;
            persistBill(bill);
            persistId = (Long) entityManager.persistAndGetId(model);
            r.delete(model);
        } else {
            persistId = (Long) entityManager.persistAndGetId(model);
            r.delete(model);
        }
        assert (!r.exists(persistId));
    }

    @Test
    public void testGetEntityFromDB(){
        Model model = ModelFactory.getModel(modelEnum);
        Model fromDB = null;
        Long persistId = null;
        if (model instanceof RoleOfUser){
            RoleOfUser roleOfUser = (RoleOfUser) model;
            persistRoleOfUser(roleOfUser);
            persistId = (Long) entityManager.persistAndGetId(model);
            fromDB = (Model)r.findOne(persistId);
        } else if (model instanceof OrderOfUser){
            OrderOfUser orderOfUser = (OrderOfUser) model;
            persistOrderOfUser(orderOfUser);
            persistId = (Long) entityManager.persistAndGetId(model);
            fromDB = (Model)r.findOne(persistId);
        } else if (model instanceof Dish){
            Dish dish = (Dish) model;
            persistDish(dish);
            persistId = (Long) entityManager.persistAndGetId(model);
            fromDB = (Model)r.findOne(persistId);
        } else if (model instanceof ChoosenDish){
            ChoosenDish choosenDish = (ChoosenDish)model;
            persistChoosenDish(choosenDish);
            persistId = (Long) entityManager.persistAndGetId(model);
            fromDB = (Model)r.findOne(persistId);
        } else if (model instanceof Bill){
            Bill bill = (Bill) model;
            persistBill(bill);
            persistId = (Long) entityManager.persistAndGetId(model);
            fromDB = (Model)r.findOne(persistId);
        } else {
            persistId = (Long) entityManager.persistAndGetId(model);
            fromDB = (Model)r.findOne(persistId);
        }
        assert (fromDB != null);
    }

    private void persistBill(Bill bill) {
        User user = entityManager.persist(bill.getUser());
        bill.setUser(user);
    }

    private void persistChoosenDish(ChoosenDish choosenDish) {
        persistOrderOfUser(choosenDish.getOrderOfUser());
        persistDish(choosenDish.getDish());
        OrderOfUser orderOfUser = entityManager.persist(choosenDish.getOrderOfUser());
        Dish dish = entityManager.persist(choosenDish.getDish());
        choosenDish.setOrderOfUser(orderOfUser);
        choosenDish.setDish(dish);
    }

    private void persistRoleOfUser(RoleOfUser roleOfUser){
        User user = entityManager.persist(roleOfUser.getUser());
        Role role = entityManager.persist(roleOfUser.getRole());
        roleOfUser.setUser(user);
        roleOfUser.setRole(role);
    }

    private void persistOrderOfUser(OrderOfUser orderOfUser){
        User user = entityManager.persist(orderOfUser.getUser());
        orderOfUser.setUser(user);
    }

    private void persistDish(Dish dish) {
        CategoryOfDish categoryOfDish = entityManager.persist(dish.getCategoryOfDish());
        dish.setCategoryOfDish(categoryOfDish);
    }



}
