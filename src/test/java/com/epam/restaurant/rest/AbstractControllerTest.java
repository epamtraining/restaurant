package com.epam.restaurant.rest;

import com.epam.restaurant.model.OrderOfUser;
import com.epam.restaurant.model.User;
import com.epam.restaurant.modelFactory.ModelEnum;
import com.epam.restaurant.modelFactory.ModelFactory;
import com.epam.restaurant.repository.OrderOfUserRepository;
import com.epam.restaurant.repository.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
@Getter
@Setter
public class AbstractControllerTest {

    protected static final String TEST_EMAIL_1 = "12345@email.ru";

    protected static final String TEST_EMAIL_2 = "54321@email.ru";

    protected static final String TEST_EMAIL_3 = "54345@email.ru";

    protected static final String TEST_EMAIL_4 = "12521@mail.ru";

    protected static final String TEST_EMAIL_5 = "125211@mail.ru";

    protected static final String TEST_EMAIL_6 = "125212@mail.ru";

    protected static final String TEST_EMAIL_7 = "125213@mail.ru";

    protected static final String TEST_EMAIL_8 = "125214@mail.ru";

    protected static final String TEST_EMAIL_9 = "125215@mail.ru";

    protected static final String TEST_EMAIL_10 = "125216@mail.ru";

    protected static final String TEST_EMAIL_11 = "125217@mail.ru";

    protected static final String TEST_EMAIL_12 = "125218@mail.ru";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderOfUserRepository orderOfUserRepository;

    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
    }

    protected OrderOfUser getOrderOfUser(String userEmail){
        OrderOfUser orderOfUser = (OrderOfUser) ModelFactory.getModel(ModelEnum.ORDER_OF_USER);
        User user = new User();
        user.setEmail(userEmail);
        orderOfUser.setUser(userRepository.save(user));
        return orderOfUserRepository.save(orderOfUser);
    }
}
