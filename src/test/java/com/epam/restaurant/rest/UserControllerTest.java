package com.epam.restaurant.rest;

import com.epam.restaurant.model.CategoryOfDish;
import com.epam.restaurant.model.Dish;
import com.epam.restaurant.modelFactory.ModelEnum;
import com.epam.restaurant.modelFactory.ModelFactory;
import com.epam.restaurant.repository.CategoryOfDishRepository;
import com.epam.restaurant.repository.DishRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserControllerTest extends AbstractControllerTest{

    private static final String TEST_URL_ALL_DISH = "/user/dish";

    private static final String TEST_URL_DISH_CATEGORY = "/user/dish/category/1";

    private static final int TEST_EXPECTED_SIZE = 1;

    @Autowired
    private DishRepository dishRepository;

    @Autowired
    private CategoryOfDishRepository categoryOfDishRepository;

    @Test
    public void testUserDish() throws Exception{
        testDish(TEST_URL_ALL_DISH, getDish(), TEST_EXPECTED_SIZE);
    }

    @Test
    public void testUserDishCategory() throws Exception{
        testDish(TEST_URL_DISH_CATEGORY, getDish(), TEST_EXPECTED_SIZE);
    }

    private void testDish(String path, Dish dish, int expectedSize) throws Exception{
            getMockMvc().perform(get(path))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(expectedSize)))
                    .andExpect(jsonPath("$[0].id", is(dish.getIdDish().intValue())))
                    .andExpect(jsonPath("$[0].nameDish", is(dish.getNameDish())))
                    .andExpect(jsonPath("$[0].description", is(dish.getDescription())))
                    .andExpect(jsonPath("$[0].prise", is(dish.getPrise())))
            ;
    }

    private Dish getDish(){
        Dish dish = (Dish) ModelFactory.getModel(ModelEnum.DISH);
        CategoryOfDish category = categoryOfDishRepository.save(dish.getCategoryOfDish());
        dish.setCategoryOfDish(category);
        return dishRepository.save(dish);
    }
}
