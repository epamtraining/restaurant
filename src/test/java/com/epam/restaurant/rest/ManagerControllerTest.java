package com.epam.restaurant.rest;

import com.epam.restaurant.model.OrderOfUser;
import com.epam.restaurant.service.OrderService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ManagerControllerTest extends AbstractControllerTest {

    private static final int WAITING_SIZE = 1;

    private static final String MANAGER_URL = "/manager/";

    private static final String MANAGER_ALL_ORDERS_URL = "/manager/order/unconfirmed";

    private static final String MANAGER_APPROVE = "/approve";

    @Autowired
    private OrderService orderService;

    @Test
    public void testListUncomfirtedOrders() throws Exception{
        getOrderOfUser(TEST_EMAIL_1);

        getMockMvc().perform(get(MANAGER_ALL_ORDERS_URL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(WAITING_SIZE)));
    }

    @Test
    public void testApproveOrder() throws Exception{
        OrderOfUser orderOfUser = getOrderOfUser(TEST_EMAIL_2);

        getMockMvc().perform(put(MANAGER_URL + orderOfUser.getIdOrder() + MANAGER_APPROVE))
                .andExpect(status().isOk());
        orderOfUser = orderService.getOrderOfUserById(orderOfUser.getIdOrder());
        assertTrue(orderOfUser.getIsConfirmed());
    }

    @Test
    public void testDeleteOrder() throws Exception{
        OrderOfUser orderOfUser = getOrderOfUser(TEST_EMAIL_3);

        getMockMvc().perform(delete(MANAGER_URL + orderOfUser.getIdOrder()))
                .andExpect(status().isOk());
        orderOfUser = orderService.getOrderOfUserById(orderOfUser.getIdOrder());
        assertTrue(orderOfUser == null);
    }



}
