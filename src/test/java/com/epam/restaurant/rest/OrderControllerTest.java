package com.epam.restaurant.rest;

import com.epam.restaurant.model.*;
import com.epam.restaurant.modelFactory.ModelEnum;
import com.epam.restaurant.modelFactory.ModelFactory;
import com.epam.restaurant.repository.*;
import com.epam.restaurant.service.OrderService;
import com.epam.restaurant.service.UserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.Charset;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OrderControllerTest extends AbstractControllerTest {

    private static final double TEST_COST = 100.0;

    private static final int EXPECTED_SIZE = 1;

    private static final String TEST_URL_USER = "/user";

    private static final String TEST_URL_BILL = "/bill/";

    private static final String TEST_URL_PAY = "/pay";

    private static final long TEST_ID_BILL = 1L;

    private static final String TEST_URL_DELETE = "/delete/";

    private static final String TEST_URL_ORDER = "/order/";

    private static final String TEST_URL_UPDATE = "/update";

    private static final String TEST_NAME = "testName";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private BillRepository billRepository;

    @Autowired
    private DishRepository dishRepository;

    @Autowired
    private OrderService orderService;

    @Autowired
    private CategoryOfDishRepository categoryOfDishRepository;

    @Autowired
    private ChoosenDishRepository choosenDishRepository;

    @Test
    public void testGetBills() throws Exception{
        createBill(TEST_EMAIL_7);
        User user = userService.loadUserByUsername(TEST_EMAIL_7);

        Authentication auth = new UsernamePasswordAuthenticationToken(user,null);

        SecurityContextHolder.getContext().setAuthentication(auth);

        getMockMvc().perform(get(TEST_URL_USER + TEST_URL_BILL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(EXPECTED_SIZE)));
    }

    @Test
    public void testGetBillByID() throws Exception{
        Bill bill = createBill(TEST_EMAIL_5);

        Authentication auth = new UsernamePasswordAuthenticationToken(bill.getUser(),null);

        SecurityContextHolder.getContext().setAuthentication(auth);

        getMockMvc().perform(get(TEST_URL_USER + TEST_URL_BILL + bill.getIdBill()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idBill", is(bill.getIdBill().intValue())));
    }

    @Test
    public void testPayBill() throws Exception{
        Bill bill = orderService.getBillByID(TEST_ID_BILL);

        Authentication auth = new UsernamePasswordAuthenticationToken(bill.getUser(),null);

        SecurityContextHolder.getContext().setAuthentication(auth);

        getMockMvc().perform(put(TEST_URL_USER + TEST_URL_BILL + bill.getIdBill() + TEST_URL_PAY))
                .andExpect(status().isOk());

        bill = orderService.getBillByID(TEST_ID_BILL);

        assertTrue(bill.getIsPaid());
    }

    @Test
    public void testGetOrdersByUserID() throws Exception{
        OrderOfUser orderOfUser = getOrderOfUser(TEST_EMAIL_10);

        Authentication auth = new UsernamePasswordAuthenticationToken(orderOfUser.getUser(),null);

        SecurityContextHolder.getContext().setAuthentication(auth);


        getMockMvc().perform(get(TEST_URL_USER + TEST_URL_ORDER))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(EXPECTED_SIZE)));
    }





    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void testGetOrderOfUserByID() throws Exception{
        OrderOfUser order = getOrderOfUser(TEST_EMAIL_11);

        Authentication auth = new UsernamePasswordAuthenticationToken(order.getUser(),null);

        SecurityContextHolder.getContext().setAuthentication(auth);

        getMockMvc().perform(get(TEST_URL_USER + TEST_URL_ORDER + order.getIdOrder()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idOrder", is(EXPECTED_SIZE)));
    }

    @Test
    public void testDeleteOrderByID() throws Exception{
        OrderOfUser order = getOrderOfUser(TEST_EMAIL_9);

        Authentication auth = new UsernamePasswordAuthenticationToken(order.getUser(),null);

        SecurityContextHolder.getContext().setAuthentication(auth);

        getMockMvc().perform(delete(TEST_URL_USER + TEST_URL_ORDER + order.getIdOrder() + TEST_URL_DELETE))
                .andExpect(status().isOk());

        OrderOfUser deletedOrder = orderService.getOrderOfUserById(order.getIdOrder());

        assertTrue(deletedOrder == null);
    }

    @Test
    public void testDeleteChoosenDishByID() throws Exception {
        OrderOfUser order = getOrderOfUser(TEST_EMAIL_12);

        long idUser = order.getUser().getIdUser();
        long idOrder = order.getIdOrder();

        CategoryOfDish categoryOfDish = new CategoryOfDish();
        categoryOfDish.setNameCategoryOfDish(TEST_NAME);
        CategoryOfDish category = categoryOfDishRepository.save(categoryOfDish);

        Dish dish = new Dish();
        dish.setNameDish(TEST_NAME);
        dish.setCategoryOfDish(category);
        Dish dishFromDB = dishRepository.save(dish);

        ChoosenDish choosenDish = new ChoosenDish();
        choosenDish.setDish(dishFromDB);
        choosenDish.setOrderOfUser(order);
        ChoosenDish choosenDishFromDB = choosenDishRepository.save(choosenDish);

        List<ChoosenDish> choosenDishesByOrderID = orderService.getChoosenDishesByOrderID(idOrder);

        int size = choosenDishesByOrderID.size();
        long idChoosenDish = choosenDishFromDB.getIdChoosenDish();

        Authentication auth = new UsernamePasswordAuthenticationToken(order.getUser(),null);

        SecurityContextHolder.getContext().setAuthentication(auth);

        getMockMvc().perform(delete(TEST_URL_USER + TEST_URL_ORDER + idOrder + TEST_URL_DELETE + idChoosenDish))
                .andExpect(status().isOk());

        assertTrue(orderService.getChoosenDishesByOrderID(idOrder).size() == --size);
    }

    @Test
    public void testAddDishIntoOrderByID() throws Exception{
        OrderOfUser orderOfUser = getOrderOfUser(TEST_EMAIL_8);

        Dish dish = (Dish)ModelFactory.getModel(ModelEnum.DISH);
        categoryOfDishRepository.save(dish.getCategoryOfDish());

        Dish save = dishRepository.save(dish);

        MediaType mediaType = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

        Authentication auth = new UsernamePasswordAuthenticationToken(orderOfUser.getUser(),null);

        SecurityContextHolder.getContext().setAuthentication(auth);

        getMockMvc().perform(put(TEST_URL_USER + TEST_URL_ORDER + orderOfUser.getIdOrder() + TEST_URL_UPDATE)
                    .contentType(mediaType)
                    .content("{ \"id\" : " + save.getIdDish() + "}"))
                .andExpect(status().isOk());

        List<ChoosenDish> list = orderService.getChoosenDishesByOrderID(orderOfUser.getIdOrder());
        assert (list.size() > 0);
    }



    private Bill createBill(String path){
        User user = new User();
        user.setEmail(path);
        User userFromDB = userRepository.save(user);
        Bill bill = (Bill)ModelFactory.getModel(ModelEnum.BILL);
        bill.setUser(userFromDB);
        bill.setCost(TEST_COST);
        bill.setIsPaid(false);
        return billRepository.save(bill);
    }




}
