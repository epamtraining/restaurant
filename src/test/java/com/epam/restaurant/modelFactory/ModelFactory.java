package com.epam.restaurant.modelFactory;

import com.epam.restaurant.model.*;

public class ModelFactory {

    private static final double TEST_COST = 10.0;
    private static final String TEST_NAME = "testName";

    public static Model getModel(ModelEnum modelEnum){
        Model model = null;
        switch (modelEnum){
            case USER: {
                model = getUser();
            } break;
            case ROLE: {
                model = getRole();
            } break;
            case ROLE_OF_USER: {
                model = getRoleOfUser();
            } break;
            case ORDER_OF_USER: {
                model = getOrderOfUser();
            } break;
            case CATEGORY_OF_DISH: {
                model = getCategoryOfDish();
            } break;
            case DISH: {
                model = getDish();
            } break;
            case CHOOSEN_DISH:{
                model = getChoosenDish();
            } break;
            case BILL:{
                model = getBill();
            } break;
        }

        return model;
    }

    private static Bill getBill() {
        Bill bill = new Bill();
        bill.setCost(TEST_COST);
        bill.setUser(getUser());
        return bill;
    }

    private static ChoosenDish getChoosenDish() {
        ChoosenDish choosenDish = new ChoosenDish();
        OrderOfUser orderOfUser = getOrderOfUser();
        Dish dish = getDish();
        choosenDish.setDish(dish);
        choosenDish.setOrderOfUser(orderOfUser);
        return choosenDish;
    }

    private static Dish getDish() {
        Dish dish = new Dish();
        dish.setNameDish(TEST_NAME);
        dish.setCategoryOfDish(getCategoryOfDish());
        return dish;
    }

    private static CategoryOfDish getCategoryOfDish() {
        CategoryOfDish categoryOfDish = new CategoryOfDish();
        categoryOfDish.setNameCategoryOfDish(TEST_NAME);
        return categoryOfDish;
    }


    private static User getUser(){
        User user = new User();
        user.setEmail(TEST_NAME);
        return user;
    }

    private static Role getRole(){
        Role role = new Role();
        role.setNameRole(TEST_NAME);
        return role;
    }

    private static RoleOfUser getRoleOfUser(){
        RoleOfUser roleOfUser = new RoleOfUser();
        Role role = getRole();
        User user = getUser();
        roleOfUser.setRole(role);
        roleOfUser.setUser(user);
        return roleOfUser;
    }

    private static OrderOfUser getOrderOfUser(){
        User user = getUser();
        OrderOfUser orderOfUser = new OrderOfUser();
        orderOfUser.setIsConfirmed(false);
        orderOfUser.setUser(user);
        return orderOfUser;
    }
}
