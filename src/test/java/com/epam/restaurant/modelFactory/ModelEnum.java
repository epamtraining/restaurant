package com.epam.restaurant.modelFactory;

public enum ModelEnum {
    USER,
    ROLE_OF_USER,
    ROLE,
    ORDER_OF_USER,
    DISH,
    CHOOSEN_DISH,
    CATEGORY_OF_DISH,
    BILL
}
