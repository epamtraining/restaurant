'use strict';

const list = document.querySelector('#orders-list');
const loadOrderBtn = document.getElementById('load-orders-button');
const cleanOrderBtn = document.getElementById('clean-orders-button');
const deleteOrdersBtn = document.getElementById('delete-orders-button');

loadOrderBtn.addEventListener('click', loadOrders, false);
cleanOrderBtn.addEventListener('click', hideOrders, false);
deleteOrdersBtn.addEventListener('click', deleteOrders, false);

function loadOrders() {
    fetch('https://jsonplaceholder.typicode.com/users', {method: 'GET'})
        .then(function(response) {
            if (response.status  === 200) {
                return response.json();
            }
            else throw response.status;
        })
        .then((data) => {
            list.innerHTML = data.reduce(function(acc, item){
                return acc + `<li class="list-group-item">
                    <input type="checkbox" id="checkbox-${item.id}">
                    <label for="checkbox-${item.id}"> ${item.address.city}</label>
                </li>`;
            }, "");
        })
        .catch(error => console.error(error));
}

function hideOrders() {
    list.innerHTML = ``;
}

function getQuery(params) {
    let acc = '?';
    params.forEach(function(value, key) {
        acc += key + '=' + value + '&';
    });
    return acc;
}

function deleteOrders() {
    const params = new Map([
        ['name', 'Valerii'],
        ['position', 'Delivery_Manager'],
        ['age', '25']
    ]);
    const query = getQuery(params);
    getChecked();
    fetch('https://jsonplaceholder.typicode.com/users' + query, {method: 'POST'})
        .then(() => {
            console.log('success!')
        })
        .catch(error => console.error(error));
}
function getChecked() {
    console.log(Array.from(document.querySelectorAll('input[type=checkbox]'))
        .filter(function(item){
            return (item.checked === true);
        }));
}

