'use strict';

const list = document.querySelector('#menu-list');
const loadMenuBtn = document.getElementById('load-menu-button');
const cleanMenuBtn = document.getElementById('clean-menu-button');

loadMenuBtn.addEventListener('click', loadMenu, false);
cleanMenuBtn.addEventListener('click', hideMenu, false);

function loadMenu() {
    fetch('https://jsonplaceholder.typicode.com/users', {method: 'GET'})
        .then(function(response) {
            if (response.status  === 200) {
                return response.json();
            }
            else throw response.status;
        })
        .then((data) => {
            list.innerHTML = data.reduce(function(acc, item){
                return acc + `<li class="list-group-item">
                    <input type="checkbox" id="checkbox-${item.id1}">
                    <label for="checkbox-${item.id1}"> ${item.address.suite}</label>
                </li>`;
            }, "");
        })
        .catch(error => console.error(error));
}

function hideMenu() {
    list.innerHTML = ``;
}