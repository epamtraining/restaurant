CREATE TABLE `role` (
  `id_role` bigint(20) NOT NULL AUTO_INCREMENT,
  `name_role` varchar(15) NOT NULL,
  PRIMARY KEY (`id_role`),
  UNIQUE KEY `uk_role_nameRole` (`name_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
  `id_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `age` int(11) DEFAULT NULL,
  `email` varchar(80) NOT NULL,
  `first_name` varchar(24) DEFAULT NULL,
  `last_name` varchar(24) DEFAULT NULL,
  `sex` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `uk_user_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `role_of_user` (
  `id_role_of_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_role` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  PRIMARY KEY (`id_role_of_user`),
--   KEY `fk_role_idRole_roleOfUser_idRole` (`id_role`),
--   KEY `fk_user_idUser_roleOfUser_idUser` (`id_user`),
  CONSTRAINT `fk_role_idRole_roleOfUser_idRole` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`),
  CONSTRAINT `fk_user_idUser_roleOfUser_idUser` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
  ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_of_user` (
  `id_order` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_confirmed` bit(1) DEFAULT NULL,
  `id_user` bigint(20) NOT NULL,
  PRIMARY KEY (`id_order`),
--   KEY `fk_user_idUser_orderOfUser_idUser` (`id_user`),
  CONSTRAINT `fk_user_idUser_orderOfUser_idUser` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
  ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `category_of_dish` (
  `id_category_of_dish` bigint(20) NOT NULL AUTO_INCREMENT,
  `name_category_of_dish` varchar(15) NOT NULL,
  PRIMARY KEY (`id_category_of_dish`),
  UNIQUE KEY `uk_categoryOfDish_nameCategoryOfDish` (`name_category_of_dish`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `dish` (
  `id_dish` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name_dish` varchar(40) NOT NULL,
  `prise` double DEFAULT NULL,
  `id_category_of_dish` bigint(20) NOT NULL,
  PRIMARY KEY (`id_dish`),
--   KEY `fk_categoryOfDish_idCategoryOfDish_dish_idCategoryOfDish` (`id_category_of_dish`),
  CONSTRAINT `fk_categoryOfDish_idCategoryOfDish_dish_idCategoryOfDish` FOREIGN KEY (`id_category_of_dish`) REFERENCES `category_of_dish` (`id_category_of_dish`)
  ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `choosen_dish` (
  `id_choosen_dish` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_dish` bigint(20) NOT NULL,
  `id_order_of_user` bigint(20) NOT NULL,
  PRIMARY KEY (`id_choosen_dish`),
--   KEY `fk_dish_idDish_choosenDish_idDish` (`id_dish`),
--   KEY `fk_orderOfUser_idOderOfUser_choosenDish_idOderOfUser` (`id_order_of_user`),
  CONSTRAINT `fk_orderOfUser_idOderOfUser_choosenDish_idOderOfUser` FOREIGN KEY (`id_order_of_user`) REFERENCES `order_of_user` (`id_order`),
  CONSTRAINT `fk_dish_idDish_choosenDish_idDish` FOREIGN KEY (`id_dish`) REFERENCES `dish` (`id_dish`)
  ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bill` (
  `id_bill` bigint(20) NOT NULL AUTO_INCREMENT,
  `cost` double NOT NULL,
  `is_paid` bit(1) DEFAULT NULL,
  `id_order_of_user` bigint(20) DEFAULT NULL,
  `id_user` bigint(20) NOT NULL,
  PRIMARY KEY (`id_bill`),
--   KEY `fk_orderOfUser_idOrderOfUser_bill_idOrderOfUser` (`id_order_of_user`),
--   KEY `fk_user_idUser_bill_idUser` (`id_user`),
  CONSTRAINT `fk_user_idUser_bill_idUser` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `fk_orderOfUser_idOrderOfUser_bill_idOrderOfUser` FOREIGN KEY (`id_order_of_user`) REFERENCES `order_of_user` (`id_order`)
  ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
