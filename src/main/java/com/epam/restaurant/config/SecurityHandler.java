package com.epam.restaurant.config;

import com.epam.restaurant.model.Role;
import com.epam.restaurant.model.RoleOfUser;
import com.epam.restaurant.model.User;
import com.epam.restaurant.service.RoleService;
import com.epam.restaurant.service.UserService;
import com.google.common.collect.Iterables;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Map;

@Log4j
@Component
@AllArgsConstructor
public class SecurityHandler extends SimpleUrlAuthenticationSuccessHandler {

    private final UserService userService;

    private final RoleService roleService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        super.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);

        User userDet = user((SecurityContextHolder
                .getContext()
                .getAuthentication()));
        log.info(userDet.getEmail() + " successfully authenticated");
        User userDetails = userService.loadUserByUsername(userDet.getEmail());

        if (userDetails != null) {
            log.info("user " + userDet.getEmail() + "is already in database");
            Authentication newAuthentication = new UsernamePasswordAuthenticationToken(
                    userDet, null, userDetails.getAuthorities());

            SecurityContextHolder.getContext().setAuthentication(newAuthentication);
            log.info(userDet.getEmail() + "logged as" + SecurityContextHolder.getContext().getAuthentication().getAuthorities());
        } else {
            log.info("user " + userDet.getEmail() + "not in database");
            userService.addUser(userDet);
            log.info("user" + userDet.getEmail() + "added to database");
        }
    }

    private User user(Principal principal) {
        if (principal != null) {
            OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) principal;
            Authentication authentication = oAuth2Authentication.getUserAuthentication();
            Map<String, String> details;
            details = (Map<String, String>) authentication.getDetails();
            User newUser = new User();
            newUser.setEmail(details.get("email"));
            newUser.setLastName(details.get("given_name"));
            newUser.setLastName(details.get("family_name"));
            newUser.setSex(details.get("gender"));

            RoleOfUser roleOfUser = new RoleOfUser();
            roleOfUser.setUser(newUser);
            Role role = roleService.findByNameRole(Iterables.getFirst(
                    authentication.getAuthorities(), "ROLE_USER").toString());
            roleOfUser.setRole(role);
            ArrayList<RoleOfUser> roles = new ArrayList<>();
            roles.add(roleOfUser);
            newUser.setRoleOfUsers(roles);
            return newUser;
        }
        return null;
    }
}
