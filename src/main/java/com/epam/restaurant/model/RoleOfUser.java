package com.epam.restaurant.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class RoleOfUser implements Model{

    @Id
    @Column(name = "id_role_of_user", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRoleOfUser;

    @ManyToOne
    @JoinColumn(name = "id_role", nullable = false)
    @JsonBackReference
    private Role role;


    @ManyToOne
    @JoinColumn(name = "id_user", nullable = false)
    @JsonBackReference
    private User user;
}
