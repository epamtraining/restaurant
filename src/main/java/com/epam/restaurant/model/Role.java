package com.epam.restaurant.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Role implements Model{

    @Id
    @Column(name = "id_role", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRole;

    @Column(name = "name_role", unique = true, nullable = false, length = 15)
    private String nameRole;

    @OneToMany(targetEntity = RoleOfUser.class, mappedBy = "role")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonManagedReference
    private List<RoleOfUser> roleOfUsers = new ArrayList<>();

}
