package com.epam.restaurant.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Bill implements Model {
    @Id
    @Column(name = "id_bill", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idBill;

    @Column(nullable = false)
    private Double cost;

    @Column(name = "is_paid")
    private Boolean isPaid;

    @ManyToOne
    @JoinColumn(name = "id_user", nullable = false)
    @JsonBackReference
    private User user;

    @OneToOne
    @JoinColumn(name = "id_order_of_user")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonBackReference
    private OrderOfUser orderOfUser;
}
