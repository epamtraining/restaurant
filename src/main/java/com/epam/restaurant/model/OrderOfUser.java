package com.epam.restaurant.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class OrderOfUser implements Model{
    @Id
    @Column(name = "id_order", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idOrder;

    @Column
    private Boolean isConfirmed;

    @ManyToOne
    @JoinColumn(name = "id_user", nullable = false)
    @JsonBackReference
    private User user;

    @OneToMany(targetEntity = ChoosenDish.class, mappedBy = "orderOfUser")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonManagedReference
    private List<ChoosenDish> choosenDishes = new ArrayList<>();

    @OneToOne(mappedBy = "orderOfUser")
    @JsonBackReference
    private Bill bill;

}
