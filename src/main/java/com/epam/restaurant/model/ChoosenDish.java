package com.epam.restaurant.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ChoosenDish implements Model{

    @Id
    @Column(name = "id_choosen_dish", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idChoosenDish;

    @ManyToOne
    @JoinColumn(name = "id_dish", nullable = false)
    @JsonBackReference
    private Dish dish;


    @ManyToOne
    @JoinColumn(name = "id_order_of_user", nullable = false)
    @JsonBackReference
    private OrderOfUser orderOfUser;

}
