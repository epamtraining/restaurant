package com.epam.restaurant.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class CategoryOfDish implements Model {

    @Id
    @Column(name = "id_category_of_dish", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCategoryOfDish;

    @Column(name = "name_category_of_dish", unique = true, nullable = false, length = 15)
    private String nameCategoryOfDish;

    @OneToMany(targetEntity = Dish.class, mappedBy = "categoryOfDish")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonManagedReference
    private List<Dish> dishes = new ArrayList<>();
}

