package com.epam.restaurant.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Dish implements Model{

    @Id
    @Column(name = "id_dish", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDish;

    @Column(name = "name_dish", nullable = false, length = 40)
    private String nameDish;

    @Column
    private String description;

    @Column
    private Double prise;

    @ManyToOne
    @JoinColumn(name = "id_category_of_dish", nullable = false)
    @JsonBackReference
    private CategoryOfDish categoryOfDish;

    @OneToMany(targetEntity = ChoosenDish.class, mappedBy = "dish")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonManagedReference
    private List<ChoosenDish> choosenDishes = new ArrayList<>();
}
