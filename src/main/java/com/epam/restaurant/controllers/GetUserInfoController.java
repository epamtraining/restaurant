package com.epam.restaurant.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class GetUserInfoController {

    @GetMapping(value = "userinfo")
    public Principal user(Principal user) {
        return user;
    }
}
