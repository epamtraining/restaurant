package com.epam.restaurant.controllers;

import com.epam.restaurant.dto.OrderOfUserDTO;
import com.epam.restaurant.model.OrderOfUser;
import com.epam.restaurant.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j
@RestController
@AllArgsConstructor
@RequestMapping("/manager")
public class ManagerController {

    private final OrderService orderService;

    @PutMapping("/{id}/approve")
    public ResponseEntity approveOrder(@PathVariable("id") Long id) {
        orderService.approveOrderById(id);
        log.info("order with id " + id + " approved");
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOrder(@PathVariable("id") Long id) {
        orderService.deleteOrderOfUserByID(id);
        log.info("order with id " + id + " deleted");
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/order/unconfirmed")
    public ResponseEntity<List<OrderOfUserDTO>> listAllOrders(){
        log.info("get list of unconfirmed orders");
        return new ResponseEntity<>(orderService.getAllUnconfirmedOrders(), HttpStatus.OK);
    }

    @PostMapping("/order/{orderId}/create")
    public ResponseEntity createBill(@PathVariable Long orderId){
        OrderOfUser order = orderService.getOrderOfUserById(orderId);
        if (!order.getIsConfirmed()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        orderService.createBill(order);
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
