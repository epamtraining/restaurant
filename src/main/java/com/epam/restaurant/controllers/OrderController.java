package com.epam.restaurant.controllers;


import com.epam.restaurant.dto.BillDTO;
import com.epam.restaurant.dto.DishDTO;
import com.epam.restaurant.dto.OrderOfUserDTO;
import com.epam.restaurant.model.Bill;
import com.epam.restaurant.model.ChoosenDish;
import com.epam.restaurant.model.OrderOfUser;
import com.epam.restaurant.model.User;
import com.epam.restaurant.service.OrderService;
import com.epam.restaurant.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Log4j
@RestController
@AllArgsConstructor
public class OrderController {

    private final OrderService orderService;

    private final UserService userService;

    @PostMapping("/user/order/create")
    public ResponseEntity createOrder(@RequestBody List<DishDTO> dishDTOS) {
        User user = userService.getUserFromAutentification();
        List<Long> listID = new ArrayList<>();
        for (DishDTO dishDTO : dishDTOS) {
            listID.add(dishDTO.getId());
        }
        orderService.addDishesIntoOrderToUser(listID, user);
        log.info("order " + dishDTOS + "created");
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/user/order")
    public ResponseEntity<List<OrderOfUserDTO>> getOrdersOfUserDTO() {
        User authUser = userService.getUserFromAutentification();
        log.info("get all user orders");
        return new ResponseEntity<>(orderService.getOrdersOfUserDTOByUser(authUser.getIdUser()), HttpStatus.OK);
    }

    @GetMapping("/user/order/{orderId}")
    public ResponseEntity<OrderOfUserDTO> getOrderOfUserDTOByIDOrder(@PathVariable Long orderId) {
        User authUser = userService.getUserFromAutentification();
        log.info("get all user orders by id" + orderId);
        if (!checkIncludedOrder(authUser.getIdUser(), orderId)) {
            log.info("disired order with id " + orderId + " does not belong to user");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(orderService.getOrderOfUserDTOByIDOrder(orderId), HttpStatus.OK);
    }

    @DeleteMapping("/user/order/{orderId}/delete")
    public ResponseEntity deleteOrder(@PathVariable Long orderId) {
        User authUser = userService.getUserFromAutentification();
        log.info("delete order by orderId" + orderId);
        if (!checkIncludedOrder(authUser.getIdUser(), orderId)) {
            log.info("disired order with id " + orderId + " does not belong to user");
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        OrderOfUser order = orderService.getOrderOfUserById(orderId);
        if (order.getIsConfirmed()) {
            log.info("disired orderwith id " + orderId + " confirmed already");
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        orderService.deleteOrderOfUserByOrder(order);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/user/order/{orderId}/delete/{idChoosenDish}")
    public ResponseEntity deleteChoosenDish(@PathVariable Long orderId, @PathVariable Long idChoosenDish) {
        User authUser = userService.getUserFromAutentification();
        log.info("delete dish by orderId" + orderId + "and idChosenDish" + idChoosenDish);
        if (!checkIncludedOrder(authUser.getIdUser(), orderId)) {
            log.info("disired order with id " + orderId + " does not belong to user");
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        OrderOfUser order = orderService.getOrderOfUserById(orderId);
        if (order.getIsConfirmed()) {
            log.info("disired order with id " + orderId + " confirmed already");
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        List<ChoosenDish> choosenDishes = orderService.getChoosenDishesByOrderID(orderId);
        ChoosenDish choosenDish = orderService.getChoosenDishByID(idChoosenDish);
        if (!choosenDishes.contains(choosenDish)){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        orderService.deleteChoosenDish(choosenDish);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/user/order/{orderId}/update")
    public ResponseEntity addDishIntoOrderByID(@PathVariable Long orderId, @RequestBody DishDTO dishDTO) {
        User authUser = userService.getUserFromAutentification();
        log.info("update order by orderId " + orderId + " and DishDTO" + dishDTO);
        if (!checkIncludedOrder(authUser.getIdUser(), orderId)) {
            log.info("order " + orderId + " does not belong to user");
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        orderService.addDishIntoOrder(orderId, dishDTO);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/user/bill")
    public ResponseEntity<List<BillDTO>> getBills() {
        User authUser = userService.getUserFromAutentification();
        log.info("get all bills");
        List<Bill> billsOfUser = orderService.getAllBillsOfUserByID(authUser.getIdUser());

        List<BillDTO> billDTOS = new ArrayList<>();
        for (Bill bill : billsOfUser) {
            BillDTO billDTO = new BillDTO(bill);
            billDTOS.add(billDTO);
        }

        return new ResponseEntity<>(billDTOS, HttpStatus.OK);
    }

    @GetMapping("/user/bill/{idBill}")
    public ResponseEntity<BillDTO> getBill(@PathVariable Long idBill) {
        User authUser = userService.getUserFromAutentification();
        log.info("get bill by idbill " + idBill);
        List<Bill> billsOfUser = orderService.getAllBillsOfUserByID(authUser.getIdUser());

        Bill billFromDB = orderService.getBillByID(idBill);
        if (!billsOfUser.contains(billFromDB)) {
            log.info("there is no bill with idbill " + idBill);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        BillDTO billDTO = new BillDTO(billFromDB);
        return new ResponseEntity<>(billDTO, HttpStatus.OK);
    }

    @PutMapping("/user/bill/{idBill}/pay")
    public ResponseEntity payBill(@PathVariable Long idBill) {
        User authUser = userService.getUserFromAutentification();
        log.info("pay bill by idbill " + idBill);
        List<Bill> billsOfUser = orderService.getAllBillsOfUserByID(authUser.getIdUser());

        Bill billFromDB = orderService.getBillByID(idBill);
        if (!billsOfUser.contains(billFromDB)) {
            log.info("there is no suchbill with idbill " + idBill);
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        orderService.payBillByID(idBill);
        return new ResponseEntity(HttpStatus.OK);
    }

    private boolean checkIncludedOrder(Long idUser, Long idOrder) {
        List<OrderOfUserDTO> ordersOfUser = orderService.getOrdersOfUserDTOByUser(idUser);
        OrderOfUserDTO orderFromPath = orderService.getOrderOfUserDTOByIDOrder(idOrder);
        return ordersOfUser.contains(orderFromPath);
    }

}