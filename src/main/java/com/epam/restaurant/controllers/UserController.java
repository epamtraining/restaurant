package com.epam.restaurant.controllers;

import com.epam.restaurant.dto.DishDTO;
import com.epam.restaurant.dto.UserDTO;
import com.epam.restaurant.model.Dish;
import com.epam.restaurant.model.User;
import com.epam.restaurant.service.DishService;
import com.epam.restaurant.service.UserService;
import com.epam.restaurant.repository.DishRepository;
import com.epam.restaurant.service.DishService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Log4j
@RestController
@AllArgsConstructor
public class UserController{

    private final DishService dishService;

    private final UserService userService;

    @GetMapping("/user/dish")
    public ResponseEntity<List<DishDTO>> getDishes(){
        log.info("get all dishes");
        List<Dish> allDishes = dishService.getAllDishes();
        List<DishDTO> dishDTOS = new ArrayList<>();
        for (Dish dish : allDishes){
            DishDTO dishDTO = new DishDTO(dish);
            dishDTOS.add(dishDTO);
        }
        return new ResponseEntity<>(dishDTOS, HttpStatus.OK);
    }

    @GetMapping("/user/dish/category/{id}")
    public ResponseEntity<List<DishDTO>> getDishesByCategory(@PathVariable Long id){
        log.info("get dishes by category " + id);
        List<Dish> allByCatogeryID = dishService.getAllByCatogeryID(id);
        List<DishDTO> dishDTOS = new ArrayList<>();
        for (Dish dish : allByCatogeryID){
            DishDTO dishDTO = new DishDTO(dish);
            dishDTOS.add(dishDTO);
        }
        return new ResponseEntity<>(dishDTOS, HttpStatus.OK);
    }

    @GetMapping("user/info")
    public ResponseEntity<UserDTO> getUserInfo(){
        log.info("get user info");
        User user = userService.getUserFromAutentification();
        UserDTO userDTO = userService.getUserDTO(user.getIdUser());
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @PutMapping("user/info/edit")
    public ResponseEntity updateUserInfo(@RequestBody UserDTO userDTO){
        log.info("change user info to " + userDTO);
        userService.updateUserByUserDTO(userDTO);
        return new ResponseEntity(HttpStatus.OK);
    }
}
