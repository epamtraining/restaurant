package com.epam.restaurant.controllers;

import com.epam.restaurant.service.RoleService;
import com.epam.restaurant.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/admin")
public class AdminController{

    private final UserService userService;
    private final RoleService roleService;

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUserById(Long id){
        userService.deleteUserByIdUser(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/{userId}/role/{role}")
    public ResponseEntity addRoleToUser(Long userId, String role) {
        roleService.addRole(userId, role);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{userId}/role/{role}")
    public ResponseEntity deleteRoleOfUser(Long userId, String role) {
        roleService.deleteRole(userId, role);
        return new ResponseEntity(HttpStatus.OK);
    }
}
