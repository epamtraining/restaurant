package com.epam.restaurant.dto;

import com.epam.restaurant.model.Dish;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DishDTO {

    private Long id;

    private String nameDish;

    private String description;

    private Double prise;

    public DishDTO(Dish dish){
        id = dish.getIdDish();
        nameDish = dish.getNameDish();
        description = dish.getDescription();
        prise = dish.getPrise();
    }




}
