package com.epam.restaurant.dto;

import com.epam.restaurant.model.User;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UserDTO {

    private Long idUser;

    private String firstName;

    private String lastName;

    private Integer age;

    private String sex;

    private String email;

    public UserDTO(User user){
        idUser = user.getIdUser();
        firstName = user.getFirstName();
        lastName = user.getLastName();
        age = user.getAge();
        sex = user.getSex();
        email = user.getEmail();
    }
}
