package com.epam.restaurant.dto;

import com.epam.restaurant.model.Bill;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class BillDTO {

    private Long idBill;

    private Double cost;

    private Boolean isPaid;

    public BillDTO(Bill bill){
        idBill = bill.getIdBill();
        cost = bill.getCost();
        isPaid = bill.getIsPaid();
    }

}
