package com.epam.restaurant.dto;

import com.epam.restaurant.model.ChoosenDish;
import com.epam.restaurant.model.OrderOfUser;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class OrderOfUserDTO {
    private Long idOrder;

    private Boolean isConfirmed;

    private List<DishDTO> dishDTOS = new ArrayList<>();

    public OrderOfUserDTO(OrderOfUser orderOfUser){
        idOrder = orderOfUser.getIdOrder();
        isConfirmed = orderOfUser.getIsConfirmed();
        for (ChoosenDish choosenDish : orderOfUser.getChoosenDishes()){
            dishDTOS.add(new DishDTO(choosenDish.getDish()));
        }
    }

}
