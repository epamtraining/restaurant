package com.epam.restaurant.service;

import com.epam.restaurant.model.Dish;
import com.epam.restaurant.repository.DishRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class DishServiceImp implements DishService {

    private final DishRepository dishRepository;

    @Override
    public List<Dish> getAllDishes() {
        return dishRepository.findAll();
    }

    @Override
    public List<Dish> getAllByCatogeryID(Long id) {
        return dishRepository.findAllByCategoryOfDish_IdCategoryOfDish(id);
    }
}
