package com.epam.restaurant.service;

import com.epam.restaurant.dto.DishDTO;
import com.epam.restaurant.dto.OrderOfUserDTO;
import com.epam.restaurant.model.Bill;
import com.epam.restaurant.model.ChoosenDish;
import com.epam.restaurant.model.OrderOfUser;
import com.epam.restaurant.model.User;

import java.util.List;

public interface OrderService {
    OrderOfUser createOrUpdateOrderOfUser(OrderOfUser orderOfUser);

    OrderOfUser getOrderOfUserById(Long id);

    List<OrderOfUser> getOrdersByUserId(Long id);

    void deleteOrderOfUserByID(Long id);

    void addDishesIntoOrderToUser(List<Long> listID, User user);

    List<OrderOfUserDTO> getOrdersOfUserDTOByUser(Long id);

    void approveOrderById(Long id);

    List<OrderOfUserDTO> getAllUnconfirmedOrders();

    void deleteOrderOfUserByOrder(OrderOfUser orderOfUser);

    OrderOfUserDTO getOrderOfUserDTOByIDOrder(Long idOrder);

    void addDishIntoOrder(Long idOrder, DishDTO dishDTO);

    List<Bill> getAllBillsOfUserByID(Long idUser);

    Bill getBillByID(Long idBill);

    void payBillByID(Long idBill);

    List<ChoosenDish> getChoosenDishesByOrderID(Long id);

    void createBill(OrderOfUser order);

    ChoosenDish getChoosenDishByID(Long id);

    void deleteChoosenDish(ChoosenDish choosenDish);
}
