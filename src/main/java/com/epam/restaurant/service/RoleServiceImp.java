package com.epam.restaurant.service;

import com.epam.restaurant.model.Role;
import com.epam.restaurant.model.RoleOfUser;
import com.epam.restaurant.model.User;
import com.epam.restaurant.repository.RoleOfUserRepository;
import com.epam.restaurant.repository.RoleRepository;
import com.epam.restaurant.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class RoleServiceImp implements RoleService {

    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final RoleOfUserRepository roleOfUserRepository;

    public Role findByNameRole(String name) {
        return roleRepository.findByNameRole(name);
    }

    public void addRole(Long userId, String roleName) {
        User user = userRepository.findByIdUser(userId);
        Role role = roleRepository.findByNameRole(roleName);
        if (user == null || role == null) {
            return;
        }
        RoleOfUser roleOfUser = roleOfUserRepository.findRoleOfUserByUserAndAndRole(user, role);
        if (roleOfUser != null) {
            return;
        }
        roleOfUser = new RoleOfUser();
        roleOfUser.setUser(user);
        roleOfUser.setRole(role);
        roleOfUserRepository.save(roleOfUser);
    }

    @Override
    public void deleteRole(Long userId, String roleName) {
        User user = userRepository.findByIdUser(userId);
        Role role = roleRepository.findByNameRole(roleName);
        if (user == null || role == null) {
            return;
        }
        RoleOfUser roleOfUser = roleOfUserRepository.findRoleOfUserByUserAndAndRole(user, role);
        if (roleOfUser == null) {
            return;
        }
        roleOfUserRepository.delete(roleOfUser);
    }
}
