package com.epam.restaurant.service;

import com.epam.restaurant.model.Dish;

import java.util.List;

public interface DishService {
    List<Dish> getAllDishes();

    List<Dish> getAllByCatogeryID(Long id);
}
