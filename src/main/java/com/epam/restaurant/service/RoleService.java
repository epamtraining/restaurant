package com.epam.restaurant.service;

import com.epam.restaurant.model.Role;
import com.epam.restaurant.model.User;

public interface RoleService {
    Role findByNameRole(String name);
    void addRole(Long userId, String role);
    void deleteRole(Long userId, String role);
}
