package com.epam.restaurant.service;

import com.epam.restaurant.dto.UserDTO;
import com.epam.restaurant.model.Role;
import com.epam.restaurant.model.RoleOfUser;
import com.epam.restaurant.model.User;
import com.epam.restaurant.repository.*;
import com.google.common.collect.Iterables;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;

@Service
@Transactional
@AllArgsConstructor
public class UserServiceImp implements UserDetailsService, UserService{

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final RoleOfUserRepository roleOfUserRepository;

    private final ChoosenDishRepository choosenDishRepository;

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username == null || username.isEmpty()){
            throw new UsernameNotFoundException("username is empty");
        }
        return findUserByEmail(username);
    }

    public User findUserByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public void addUser(User newUser){
        String role = Iterables.getFirst(newUser.getAuthorities(), "ROLE_USER").toString();

        Role roleFromDB = roleRepository.findByNameRole(role);

        User userSaved = userRepository.save(newUser);

        RoleOfUser roleOfUser = new RoleOfUser();

        roleOfUser.setRole(roleFromDB);
        roleOfUser.setUser(userSaved);
        roleOfUserRepository.save(roleOfUser);
    }

    @Override
    public UserDTO getUserDTO(Long id) {
        User user = userRepository.findByIdUser(id);
        return new UserDTO(user);
    }

    @Override
    public void updateUserByUserDTO(UserDTO userDTO) {
        User user = getUserFromAutentification();
        user.setAge(userDTO.getAge());
        user.setSex(userDTO.getSex());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        userRepository.save(user);
    }

    @Override
    public void deleteUserByIdUser(Long id) {
        userRepository.deleteByIdUser(id);
    }

    @Override
    public User getUserByID(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User getUserFromAutentification() {
        Principal principal = SecurityContextHolder
                .getContext()
                .getAuthentication();
        return findUserByEmail(principal.getName());
    }
}
