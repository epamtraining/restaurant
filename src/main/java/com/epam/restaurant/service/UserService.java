package com.epam.restaurant.service;

import com.epam.restaurant.dto.UserDTO;
import com.epam.restaurant.model.User;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserService {
    User getUserByID(Long id);

    User loadUserByUsername(String username);

    User findUserByEmail(String email);

    void addUser(User user);

    UserDTO getUserDTO(Long id);

    void updateUserByUserDTO(UserDTO userDTO);

    void deleteUserByIdUser(Long id);

    User getUserFromAutentification();
}
