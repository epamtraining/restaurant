package com.epam.restaurant.service;

import com.epam.restaurant.dto.DishDTO;
import com.epam.restaurant.dto.OrderOfUserDTO;
import com.epam.restaurant.model.*;
import com.epam.restaurant.repository.BillRepository;
import com.epam.restaurant.repository.ChoosenDishRepository;
import com.epam.restaurant.repository.DishRepository;
import com.epam.restaurant.repository.OrderOfUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderOfUserRepository orderOfUserRepository;

    private final ChoosenDishRepository choosenDishRepository;

    private final DishRepository dishRepository;

    private final BillRepository billRepository;

    @Override
    public OrderOfUser createOrUpdateOrderOfUser(OrderOfUser orderOfUser) {
        return orderOfUserRepository.save(orderOfUser);
    }

    @Override
    public OrderOfUser getOrderOfUserById(Long id) {
        return orderOfUserRepository.findOne(id);
    }

    @Override
    public List<OrderOfUser> getOrdersByUserId(Long id) {
        return orderOfUserRepository.findAllByUser_IdUser(id);
    }

    @Override
    public void deleteOrderOfUserByID(Long id) {
        OrderOfUser order = orderOfUserRepository.findByIdOrder(id);
        List<ChoosenDish> choosenDishes = order.getChoosenDishes();
        choosenDishRepository.delete(choosenDishes);
        orderOfUserRepository.delete(id);
    }

    @Override
    public void addDishesIntoOrderToUser(List<Long> listID, User user) {
        OrderOfUser orderOfUser = new OrderOfUser();
        orderOfUser.setUser(user);
        orderOfUser.setIsConfirmed(false);
        orderOfUser = createOrUpdateOrderOfUser(orderOfUser);
        List<Dish> dishes = dishRepository.findAllByIdDishIn(listID);
        List<ChoosenDish> choosenDishes = new ArrayList<>();
        for (Dish dish : dishes){
            ChoosenDish choosenDish = new ChoosenDish();
            choosenDish.setOrderOfUser(orderOfUser);
            choosenDish.setDish(dish);
            choosenDishes.add(choosenDish);
        }
        choosenDishRepository.save(choosenDishes);
    }

    @Override
    public List<OrderOfUserDTO> getOrdersOfUserDTOByUser(Long id) {
        List<OrderOfUserDTO> orderOfUserDTOS = new ArrayList<>();
        List<OrderOfUser> orders = getOrdersByUserId(id);
        for (OrderOfUser orderOfUser : orders){
            orderOfUserDTOS.add(new OrderOfUserDTO(orderOfUser));
        }
        return orderOfUserDTOS;
    }

    @Override
    public void approveOrderById(Long id) {
        OrderOfUser orderOfUser = getOrderOfUserById(id);
        orderOfUser.setIsConfirmed(true);
        orderOfUserRepository.save(orderOfUser);
    }

    @Override
    public List<OrderOfUserDTO> getAllUnconfirmedOrders() {
        List<OrderOfUser> allUncomfirtedOrders = orderOfUserRepository.findAllByIsConfirmed(false);
        List<OrderOfUserDTO> orderOfUserDTOS = new ArrayList<>();
        for (OrderOfUser order : allUncomfirtedOrders){
            OrderOfUserDTO orderOfUserDTO = new OrderOfUserDTO(order);
            orderOfUserDTOS.add(orderOfUserDTO);
        }
        return orderOfUserDTOS;
    }

    @Override
    public void deleteOrderOfUserByOrder(OrderOfUser orderOfUser) {
        List<ChoosenDish> choosenDishes = orderOfUser.getChoosenDishes();
        choosenDishRepository.delete(choosenDishes);
        orderOfUserRepository.delete(orderOfUser);
    }

    @Override
    public OrderOfUserDTO getOrderOfUserDTOByIDOrder(Long idOrder) {
        OrderOfUser order = orderOfUserRepository.findByIdOrder(idOrder);
        return new OrderOfUserDTO(order);
    }

    @Override
    public void addDishIntoOrder(Long idOrder, DishDTO dishDTO) {
        OrderOfUser order = orderOfUserRepository.findByIdOrder(idOrder);
        ChoosenDish choosenDish = new ChoosenDish();
        choosenDish.setOrderOfUser(order);
        choosenDish.setDish(dishRepository.findByidDish(dishDTO.getId()));
        choosenDishRepository.save(choosenDish);
    }

    @Override
    public List<Bill> getAllBillsOfUserByID(Long idUser) {
        return billRepository.findAllByUser_IdUser(idUser);
    }

    @Override
    public Bill getBillByID(Long idBill) {
        return billRepository.findByIdBill(idBill);
    }

    @Override
    public void payBillByID(Long idBill) {
        Bill bill = billRepository.findByIdBill(idBill);
        bill.setIsPaid(true);
        billRepository.save(bill);
    }

    @Override
    public List<ChoosenDish> getChoosenDishesByOrderID(Long id) {
        return choosenDishRepository.findAllByOrderOfUser_IdOrder(id);
    }

    @Override
    public void createBill(OrderOfUser order) {
        Bill bill = new Bill();
        List<ChoosenDish> choosenDishes = order.getChoosenDishes();
        List<Dish> dishes = new ArrayList<>();
        for (ChoosenDish choosenDish : choosenDishes){
            dishes.add(choosenDish.getDish());
        }
        double sum = sumCost(dishes);
        bill.setCost(sum);
        bill.setIsPaid(false);
        bill.setUser(order.getUser());
        bill.setOrderOfUser(order);
        billRepository.save(bill);
    }

    @Override
    public ChoosenDish getChoosenDishByID(Long id) {
        return choosenDishRepository.findByIdChoosenDish(id);
    }

    @Override
    public void deleteChoosenDish(ChoosenDish choosenDish) {
        choosenDishRepository.delete(choosenDish);
    }

    private double sumCost(List<Dish> dishes){
        double sum = 0;
        for (Dish dish : dishes){
            sum += dish.getPrise();
        }
        return sum;
    }
}
