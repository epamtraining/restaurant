package com.epam.restaurant.repository;

import com.epam.restaurant.model.Bill;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(propagation = Propagation.MANDATORY)
public interface BillRepository extends PagingAndSortingRepository<Bill, Long> {

    List<Bill> findAllByUser_IdUser(Long idUser);

    Bill findByIdBill(Long idBill);
}
