package com.epam.restaurant.repository;

import com.epam.restaurant.model.ChoosenDish;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(propagation = Propagation.MANDATORY)
public interface ChoosenDishRepository extends CrudRepository<ChoosenDish, Long> {

    ChoosenDish findByIdChoosenDish(Long idChoosenDish);

    List<ChoosenDish> findAllByOrderOfUser_IdOrder(Long idOrderOfUser);
}
