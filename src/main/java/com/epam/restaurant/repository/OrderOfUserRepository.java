package com.epam.restaurant.repository;

import com.epam.restaurant.model.OrderOfUser;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(propagation = Propagation.MANDATORY)
public interface OrderOfUserRepository extends PagingAndSortingRepository<OrderOfUser, Long> {
    List<OrderOfUser> findAllByUser_IdUser(Long id);

    List<OrderOfUser> findAllByIsConfirmed(boolean confirmed);

    OrderOfUser findByIdOrder(Long id);
}
