package com.epam.restaurant.repository;

import com.epam.restaurant.model.Dish;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(propagation = Propagation.MANDATORY)
public interface DishRepository extends PagingAndSortingRepository<Dish, Long> {
    List<Dish> findAllByCategoryOfDish_IdCategoryOfDish(Long id);

    Dish findByidDish(Long id);

    List<Dish> findAll();

    List<Dish> findAllByIdDishIn(List<Long> id);
}
