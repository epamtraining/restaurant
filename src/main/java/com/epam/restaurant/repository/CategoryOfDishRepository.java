package com.epam.restaurant.repository;

import com.epam.restaurant.model.CategoryOfDish;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.MANDATORY)
public interface CategoryOfDishRepository extends CrudRepository<CategoryOfDish, Long> {
}
