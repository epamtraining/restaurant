package com.epam.restaurant.repository;

import com.epam.restaurant.model.Role;
import com.epam.restaurant.model.RoleOfUser;
import com.epam.restaurant.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.MANDATORY)
public interface RoleOfUserRepository extends CrudRepository<RoleOfUser, Long> {

    RoleOfUser findRoleOfUserByUserAndAndRole(User user, Role role);
}
