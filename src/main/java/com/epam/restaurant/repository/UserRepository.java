package com.epam.restaurant.repository;

import com.epam.restaurant.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.MANDATORY)
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    User findByEmail(String email);

    User findByIdUser(Long id);

    void deleteByIdUser(Long id);
}
