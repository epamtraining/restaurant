package com.epam.restaurant.repository;

import com.epam.restaurant.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.MANDATORY)
public interface RoleRepository extends CrudRepository<Role, Long> {
    Role findByNameRole(String name);
}
