# MySQL Setup Instruction
## 1. Download MySQL Installer: 
    https://dev.mysql.com/downloads/installer/
## 2. Setup MySQL
    Choose Developer default and install
## 3. Configure server after all components were installed
    a.	Choose Standalone MySQL Server / Classic MySQL Replication
    b.	Think up root password and remember it. Add User. 
    c.	Next, next and next…
## 4. MySQL is installed. 

# Configuration of connection to app
## 1. MySQL
    a. Open MySQL Workbench
    b. Connect to local instance
    c. Create schema. Name is restaurant
## 2. Project
    a. Open file application.yml in the project
    b. Change username and password to yours